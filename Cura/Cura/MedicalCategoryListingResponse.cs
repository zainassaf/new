﻿using System.Collections.Generic;

public class Doctor
{
    public int Id { get; set; }
    public int DocId { get; set; }
    public string UserName { get; set; }
    public string FirstName { get; set; }
    public string FirstName_ar { get; set; }
    public string LastName { get; set; }
    public string LastName_ar { get; set; }
    public string SpecialtyTitle_en { get; set; }
    public string SpecialtyTitle_ar { get; set; }
    public string SpecialtyTitle_tr { get; set; }
    public string Title_en { get; set; }
    public int LocationCountryId { get; set; }
    public int LocationCityId { get; set; }
    public string ClassificationTitle_ar { get; set; }
    public string ClassificationTitle_en { get; set; }
    public string ClassificationTitle_tr { get; set; }
    public int Rating { get; set; }
    public string ProfilePic { get; set; }
    public string ProfilePicThumbnail { get; set; }
    public bool isPremium { get; set; }
    public int PrimarySpecialtyId { get; set; }
    public int AvailabilityId { get; set; }
    public bool AvailabilityAcceptsNewConsults { get; set; }
    public bool AvailabilityFlagWithOnlineIcon { get; set; }
    public string AvailabilityStatusColor { get; set; }
    public string AvailabilityStatusTitleDoctor_en { get; set; }
    public string AvailabilityStatusTitleDoctor_ar { get; set; }
    public string AvailabilityStatusTitleDoctor_tr { get; set; }
    public string AvailabilityStatusTitleUser_en { get; set; }
    public string AvailabilityStatusTitleUser_ar { get; set; }
    public string AvailabilityStatusTitleUser_tr { get; set; }
    public bool DesignateAsDoctor { get; set; }
    public string LastLoginAttempt { get; set; }
    public bool Featured { get; set; }
}

public class Result
{
    public int Id { get; set; }
    public int Order { get; set; }
    public string Icon { get; set; }
    public string Title_ar { get; set; }
    public string Title_en { get; set; }
    public string Title_tr { get; set; }
    public List<Doctor> Doctors { get; set; }
}

public class ResponseStatus
{
    public string ErrorCode { get; set; }
}

public class MedicalCategoryListingResponse
{
    public List<Result> Result { get; set; }
    public ResponseStatus ResponseStatus { get; set; }
}