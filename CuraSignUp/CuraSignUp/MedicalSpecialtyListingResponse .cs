﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Deserializers;
using RestSharp.Serializers;

//namespace CuraSignUp
//{
//   public class MedicalSpecialtyListingResponse
//    {
//        [JsonProperty ("Result")]
//        public List<MedicalSpecialtyDto> Result { get; set; }
//        //public ResponseStatus ResponseStatus { get; set; }

//    }
//  public  class MedicalSpecialtyDto
//    {
//        public long Id { get; set; }
//        public string Title_ar { get; set; }
//        public string Title_en { get; set; }
//        public string Title_tr { get; set; }
//        public long ParentId { get; set; }
//        //public string Title { get; set; }
//    }
//    class Class1
//    {
//        public MedicalSpecialtyListingResponse p = new MedicalSpecialtyListingResponse();
//        RestClient client = new RestClient("http://api-dev.cura.healthcare/");
//        RestRequest request = new RestRequest("MedicalSpeicialty/0", Method.GET);
//        public void Main()
//        {
//            request.RequestFormat = DataFormat.Json;
//            var response = client.Execute(request);
//            var content = response.Content;
//            var v = JsonConvert.DeserializeObject<List<MedicalSpecialtyListingResponse>>(content);
//            List<MedicalSpecialtyListingResponse> l = new List<MedicalSpecialtyListingResponse>();

//        }
//    }
//}
namespace QuickType
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class MedicalSpecialtyListingResponse
    {
        [JsonProperty("Result")]
        public Result[] Result { get; set; }

        [JsonProperty("ResponseStatus")]
        public ResponseStatus ResponseStatus { get; set; }
    }

    public partial class ResponseStatus
    {
        [JsonProperty("ErrorCode")]
        public string ErrorCode { get; set; }
    }

    public partial class Result
    {
        [JsonProperty("Id")]
        public long Id { get; set; }

        [JsonProperty("Title_ar")]
        public string TitleAr { get; set; }

        [JsonProperty("Title_en")]
        public string TitleEn { get; set; }

        [JsonProperty("Title_tr")]
        public string TitleTr { get; set; }

        [JsonProperty("ParentId")]
        public long ParentId { get; set; }
    }

    public partial class MedicalSpecialtyListingResponse
    {
        public static MedicalSpecialtyListingResponse FromJson(string json) => JsonConvert.DeserializeObject<MedicalSpecialtyListingResponse>(json, QuickType.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this MedicalSpecialtyListingResponse self) => JsonConvert.SerializeObject(self, QuickType.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
