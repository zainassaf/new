﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;
using RestSharp;

namespace Cura
{
    [Activity(Label = "FilterActivity")]
    public class FilterActivity : Activity
    {
        Spinner s1;
        Spinner s2;
        Spinner s3;
        SearchView sv1;
        List<Doctor> doctor = new List<Doctor>();
        String[] docs;
        String[] docc;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.Filter);

            RestClient client = new RestClient("http://api-dev.cura.healthcare/");
            RestRequest request = new RestRequest("MedicalCategoriesListing/193", Method.GET);
            request.RequestFormat = DataFormat.Json;
            var response = client.Execute<List<MedicalCategoryListingResponse>>(request);
            var content = response.Content;
            var eventResponse = JsonConvert.DeserializeObject<MedicalCategoryListingResponse>(content);
            foreach (var i in eventResponse.Result)
            {
                foreach (var j in i.Doctors)
                {
                    doctor.Add(j);
                }
            }

            s1 = FindViewById<Spinner>(Resource.Id.spinner1);
            s2 = FindViewById<Spinner>(Resource.Id.spinner2);
            s3 = FindViewById<Spinner>(Resource.Id.spinner3);
            sv1 = FindViewById<SearchView>(Resource.Id.searchView1);
            var adapter1 = new ArrayAdapter(this,Android.Resource.Layout.SimpleSpinnerDropDownItem,docs);
            s1.Adapter = adapter1;
            var adapter2 = new ArrayAdapter(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, docc);
            s2.Adapter = adapter2;

        }
    }
}