﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Support.V7.App;
using Android.Views;
using Android.Graphics;
using System;
using System.Text.RegularExpressions;
using Android.Runtime;
using static System.Net.Mime.MediaTypeNames;
using Java.Util.Regex;
using System.Linq;
using RestSharp;
using Android.Content;

namespace CuraSignUp
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        string fullNameEN;
        string fullNameAR;
        string userName;
        string email;
        string password;
        string midschool;
        string licnum;
        string ok;
        string please1;
        string warning;
        string nationality;
        string licenseCountry;
        string primarySpecialty;
        string degree;
        string year;
        string gender;
        string passwords;
        string usernames;
        string emails;
        string arabun;
        string englishun;
        string pleasefill;
        string done;
        
      

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.activity_main);
            Button button = FindViewById<Button>(Resource.Id.button1);
            this.FindViewById<Button>(Resource.Id.button1).Click += this.Message;


            ok = GetString(Resource.String.Ok);
            please1 = GetString(Resource.String.Please1);
            warning = GetString(Resource.String.Warning);
            nationality = GetString(Resource.String.Nationality);
            licenseCountry = GetString(Resource.String.LicenseCountry);
            primarySpecialty= GetString(Resource.String.PrimarySpecialty);
            degree = GetString(Resource.String.Degree);
            year = GetString(Resource.String.Year);
            gender = GetString(Resource.String.Gender);
            passwords = GetString(Resource.String.Password);
            usernames = GetString(Resource.String.User_Name);
            emails = GetString(Resource.String.Email);
            arabun = GetString(Resource.String.Full_Name_Arabic);
            englishun = GetString(Resource.String.Full_Name_English);
            pleasefill = GetString(Resource.String.PleaseFillupAllyourInformation);
            done = GetString(Resource.String.done);

            Spinner spinner = FindViewById<Spinner>(Resource.Id.spinner1);

            spinner.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(spinner1_ItemSelected);
            var adapter = ArrayAdapter.CreateFromResource(
                    this, Resource.Array.Genders_Array, Android.Resource.Layout.SimpleSpinnerItem);

            adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            spinner.Adapter = adapter;
            // spinner2
            Spinner spinner2 = FindViewById<Spinner>(Resource.Id.spinner2);

            spinner2.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(spinner2_ItemSelected);
            var adapter2 = ArrayAdapter.CreateFromResource(
                    this, Resource.Array.Years_Array, Android.Resource.Layout.SimpleSpinnerItem);

            adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            spinner2.Adapter = adapter2;

            // spinner3
            Spinner spinner3 = FindViewById<Spinner>(Resource.Id.spinner3);

            spinner3.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(spinner3_ItemSelected);
            var adapter3 = ArrayAdapter.CreateFromResource(
                    this, Resource.Array.Degree_Array, Android.Resource.Layout.SimpleSpinnerItem);

            adapter3.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            spinner3.Adapter = adapter3;
            // spinner 4
            Spinner spinner4 = FindViewById<Spinner>(Resource.Id.spinner4);

            spinner4.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(spinner4_ItemSelected);
            var adapter4 = ArrayAdapter.CreateFromResource(
                    this, Resource.Array.PS_Array, Android.Resource.Layout.SimpleSpinnerItem);

            adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            spinner4.Adapter = adapter4;
            //spinner5
            Spinner spinner5 = FindViewById<Spinner>(Resource.Id.spinner5);

            spinner5.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(spinner5_ItemSelected);
            var adapter5 = ArrayAdapter.CreateFromResource(
                    this, Resource.Array.Countries_Array, Android.Resource.Layout.SimpleSpinnerItem);

            adapter5.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            spinner5.Adapter = adapter5;
            //spinner6
            Spinner spinner6 = FindViewById<Spinner>(Resource.Id.spinner6);

            spinner6.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(spinner6_ItemSelected);
            var adapter6 = ArrayAdapter.CreateFromResource(
                    this, Resource.Array.Countries2_Array, Android.Resource.Layout.SimpleSpinnerItem);

            adapter6.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            spinner6.Adapter = adapter6;

        }
        private void Message(object sender, EventArgs e)
        {
            fullNameEN = FindViewById<EditText>(Resource.Id.textView1).Text;
            fullNameAR = FindViewById<EditText>(Resource.Id.textView2).Text;
            userName = FindViewById<EditText>(Resource.Id.textView3).Text;
            email = FindViewById<EditText>(Resource.Id.textView4).Text;
            password = FindViewById<EditText>(Resource.Id.editText1).Text;
            midschool = FindViewById<EditText>(Resource.Id.textView5).Text;
            licnum = FindViewById<EditText>(Resource.Id.textView6).Text;

            Register doc = new Register
            {
                UserName = userName,
                FirstName = fullNameEN.Substring(0, fullNameEN.IndexOf(" ")),
                LastName = fullNameEN.Substring(fullNameEN.IndexOf(" ") + 1),
                DisplayName = fullNameEN,
                Email = email,
                Password = password,
                AutoLogin = true,
                Continue = ""
            };

            string msg = "";
            if (fullNameEN.Equals("") &&
                fullNameAR.Equals("") &&
                userName.Equals("") &&
                email.Equals("") &&
                password.Equals("") &&
                midschool.Equals("") &&
                licnum.Equals("") &&
                FindViewById<Spinner>(Resource.Id.spinner1).SelectedItem.ToString().Equals(gender) &&
                FindViewById<Spinner>(Resource.Id.spinner2).SelectedItem.ToString().Equals(year) &&
                FindViewById<Spinner>(Resource.Id.spinner3).SelectedItem.ToString().Equals(degree) &&
                FindViewById<Spinner>(Resource.Id.spinner4).SelectedItem.ToString().Equals(primarySpecialty) &&
                FindViewById<Spinner>(Resource.Id.spinner5).SelectedItem.ToString().Equals(licenseCountry) &&
                FindViewById<Spinner>(Resource.Id.spinner6).SelectedItem.ToString().Equals(nationality))
            {
                Android.App.AlertDialog.Builder builder;
                builder = new Android.App.AlertDialog.Builder(this);
                builder.SetTitle(warning);
                builder.SetMessage(pleasefill);
                builder.SetCancelable(false);
                builder.SetPositiveButton(ok, delegate { });
                Dialog dialog = builder.Create();
                dialog.Show();
            }
            else
            {

                if (Validfullname(fullNameEN).Equals(true))
                {

                }
                else

                {
                    msg += " " + englishun;
                }
                if (Validfullnamear(fullNameAR).Equals(true))
                {

                }
                else
                {
                    msg += ", " + arabun;
                }
                if (Validemail(email).Equals(true))
                {

                }
                else
                {
                    msg += ", " + emails;
                }
                if (Validusername(userName).Equals(true))
                {

                }
                else
                {
                    msg += ", " + usernames;
                }
                if (Validpassword(password).Equals(true))
                {

                }
                else
                {
                    msg += ", " + passwords;
                }
                if (FindViewById<Spinner>(Resource.Id.spinner1).SelectedItem.ToString().Equals(gender))
                {
                    msg += ", " + gender;
                }
                if (FindViewById<Spinner>(Resource.Id.spinner2).SelectedItem.ToString().Equals(year))
                {
                    msg += ", " + year;
                }
                if (FindViewById<Spinner>(Resource.Id.spinner3).SelectedItem.ToString().Equals(degree))
                {
                    msg += ", " + degree;
                }
                if (FindViewById<Spinner>(Resource.Id.spinner4).SelectedItem.ToString().Equals(primarySpecialty))
                {
                    msg += ", " + primarySpecialty;
                }
                if (FindViewById<Spinner>(Resource.Id.spinner5).SelectedItem.ToString().Equals(licenseCountry))
                {
                    msg += ", " + licenseCountry;
                }
                if (FindViewById<Spinner>(Resource.Id.spinner6).SelectedItem.ToString().Equals(nationality))
                {
                    msg += ", " + nationality;
                }

            }
            if (Validfullname(FindViewById<EditText>(Resource.Id.textView1).Text.ToString()).Equals(true) &&
                Validfullnamear(FindViewById<EditText>(Resource.Id.textView2).Text.ToString()).Equals(true) &&
                Validemail(FindViewById<EditText>(Resource.Id.textView4).Text.ToString()).Equals(true) &&
                Validusername(FindViewById<EditText>(Resource.Id.textView3).Text.ToString()).Equals(true) &&
                Validpassword(FindViewById<EditText>(Resource.Id.editText1).Text.ToString()).Equals(true) &&
                FindViewById<Spinner>(Resource.Id.spinner1).SelectedItem.ToString() != (gender) &&
                FindViewById<Spinner>(Resource.Id.spinner2).SelectedItem.ToString() != (year) &&
                FindViewById<Spinner>(Resource.Id.spinner3).SelectedItem.ToString() != (degree) &&
                FindViewById<Spinner>(Resource.Id.spinner4).SelectedItem.ToString() != (primarySpecialty) &&
                FindViewById<Spinner>(Resource.Id.spinner5).SelectedItem.ToString() != (licenseCountry) &&
                FindViewById<Spinner>(Resource.Id.spinner6).SelectedItem.ToString() != (nationality)
                )
            {
                Android.App.AlertDialog.Builder builder;
                builder = new Android.App.AlertDialog.Builder(this);
                builder.SetTitle("");
                builder.SetMessage(done);
                builder.SetCancelable(false);
                builder.SetPositiveButton(ok, delegate { });
                Dialog dialog = builder.Create();
                dialog.Show();



                var client = new RestClient("http://api-dev.cura.healthcare");
                var request = new RestRequest("/Register", Method.POST);
                request.AddObject(doc);
                request.AddHeader("Accept", "application/json");
                IRestResponse MyCallback = client.Execute(request);
                var content = MyCallback.Content;
                Intent nextActivity = new Intent(this, typeof(Activity2));
                StartActivity(nextActivity);
            }
            else if (FindViewById<Spinner>(Resource.Id.spinner6).SelectedItem.ToString() != (nationality))
            {
                Android.App.AlertDialog.Builder builder;
                builder = new Android.App.AlertDialog.Builder(this);
                builder.SetTitle(warning);
                builder.SetMessage(please1 + msg);
                builder.SetCancelable(false);
                builder.SetPositiveButton(ok, delegate { });
                Dialog dialog = builder.Create();
                dialog.Show();

            }
        }

        private void spinner1_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            Spinner spinner = (Spinner)sender;
        }
        private void spinner2_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            Spinner spinner = (Spinner)sender;
        }
        private void spinner3_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            Spinner spinner = (Spinner)sender;
        }
        private void spinner4_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            Spinner spinner = (Spinner)sender;
        }
        private void spinner5_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            Spinner spinner = (Spinner)sender;
        }
        private void spinner6_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            Spinner spinner = (Spinner)sender;
        }
        public bool isValidEmail(string email)
        {
            Regex regex = new Regex(@"^[A - Za - z][A - Za - z0 - 9] * ([._ -]?[A - Za - z0 - 9] +)@[A - Za - z].[A - Za - z]{ 0,3}?.[A-Za-z]{0,2}$");
            return regex.IsMatch(email);
        }

        public bool Validemail(string email)
        {
            Regex EmailRegex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            return EmailRegex.IsMatch(email);
        }
        public bool Validpassword(string password)
        {
            Regex PasswordRegex = new Regex(@"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$");
            return PasswordRegex.IsMatch(password);
        }
        public bool Validusername(string username)
        {
            Regex UsernameRegex = new Regex(@"^[a-z0-9_-]{3,15}$");
            return UsernameRegex.IsMatch(username);
        }
        public bool Validfullname(string fullname)
        {
            Regex FullnameRegex = new Regex(@"^([a-zA-Z]{2,}\s[a-zA-z]{1,}'?-?[a-zA-Z]{2,}\s?([a-zA-Z]{1,})?)");
            Regex fullname2Regex = new Regex(@"^[\x20-\x7E]*$");
            return FullnameRegex.IsMatch(fullname) && fullname2Regex.IsMatch(fullname);
        }
        public bool Validfullnamear(string fullnamear)
        {
            Regex fullnamearRegex = new Regex(@"^[\u0621-\u064A]+(\s[\u0621-\u064A]+)$");
            return fullnamearRegex.IsMatch(fullnamear);
        }

    }
}

