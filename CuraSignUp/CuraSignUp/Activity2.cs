﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Text;
using Android.Views;
using Android.Widget;
using Java.Util;
using Newtonsoft.Json;
using QuickType;
using RestSharp;

namespace CuraSignUp
{
    [Activity(Label = "Activity2")]
    public class Activity2 : Activity
    {
       
        AutoCompleteTextView t;
        Spinner s;
        List<string> list1 = new List<string>();


        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.SList);
            //Activity ac = new Activity();
            //LayoutInflater inflater = (LayoutInflater)ac.GetSystemService(Context.LayoutInflaterService);
            //var convertView = inflater.Inflate(Resource.Layout.SList, null);
            //ten.Add(s.Result[0].Title_en);

            t = FindViewById<AutoCompleteTextView>(Resource.Id.autoCompleteTextView2);
            s = FindViewById<Spinner>(Resource.Id.spinner9);
            t.TextChanged += onTextChanged;
            RestClient client = new RestClient("http://api-dev.cura.healthcare/");
            RestRequest request = new RestRequest("MedicalSpeicialty/0", Method.GET);
            request.RequestFormat = DataFormat.Json;
            var response = client.Execute<List<MedicalSpecialtyListingResponse>>(request);
            var content = response.Content;
            var myObject = MedicalSpecialtyListingResponse.FromJson(content);
            for (int i = 0; i <=99; i++)
            {
                if (CultureInfo.CurrentCulture.Name.Contains("en"))
                {
                    list1.Add(myObject.Result[i].TitleEn.ToString());
                }
                else if (CultureInfo.CurrentCulture.Name.Contains("ar"))
                {
                    list1.Add(myObject.Result[i].TitleAr.ToString());
                }
                else if (CultureInfo.CurrentCulture.Name.Contains("tr"))
                {
                    list1.Add(myObject.Result[i].TitleTr.ToString());
                }

            }
                ArrayAdapter<string> adapter = new ArrayAdapter<string>(this, Resource.Layout.support_simple_spinner_dropdown_item, list1);
                t.Adapter = adapter;
                s.Adapter = adapter;
            //t.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(_ItemSelected);
            //s.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(_ItemSelected);
            //t.TextChanged += new EventHandler<TextChangedEventArgs>(onTextChanged);
            

            

        }

        private void onTextChanged(object sender, TextChangedEventArgs e)
        {
            //foreach (string h in list1)
            //{
            //    if (t.Text.Equals(h))
            //    {
            //        s.SetSelection(list1.IndexOf(h));
            //    }
            //}
        }


    }
}