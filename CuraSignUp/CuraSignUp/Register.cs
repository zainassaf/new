﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using RestSharp;


namespace CuraSignUp
{
    public class Register 
    {
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DisplayName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool AutoLogin { get; set; }
        public string Continue { get; set; }
    }
    
}