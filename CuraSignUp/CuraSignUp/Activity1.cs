﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace CuraSignUp
{
    [Activity(Label = "Cura كيورا", Theme = "@style/AppTheme")]
    public class Activity1 : Activity
    {
        
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.LanguageSwitcher);
            Button button1 = FindViewById<Button>(Resource.Id.button1);
            Button button2 = FindViewById<Button>(Resource.Id.button2);
            Button button3 = FindViewById<Button>(Resource.Id.button3);
            this.FindViewById<Button>(Resource.Id.button1).Click += this.Onclick1;
            this.FindViewById<Button>(Resource.Id.button2).Click += this.Onclick2;
            this.FindViewById<Button>(Resource.Id.button3).Click += this.Onclick3;
        }

        private void Onclick1(object sender, EventArgs e)
        {
            var locale = new Java.Util.Locale("en");
            Android.Content.Res.Configuration conf = Resources.Configuration;
            conf.Locale = locale;
            DisplayMetrics dm = Resources.DisplayMetrics;
#pragma warning disable CS0618 // Type or member is obsolete
            Resources.UpdateConfiguration(conf, dm);
#pragma warning restore CS0618 // Type or member is obsolete
            Recreate();
            System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("en");
            Intent nextActivity = new Intent(this, typeof(MainActivity));
            StartActivity(nextActivity);
        }

        private void Onclick2(object sender, EventArgs e)
        {
            var locale = new Java.Util.Locale("ar");
            Android.Content.Res.Configuration conf = Resources.Configuration;
            conf.Locale = locale;
            DisplayMetrics dm = Resources.DisplayMetrics;
#pragma warning disable CS0618 // Type or member is obsolete
            Resources.UpdateConfiguration(conf, dm);
#pragma warning restore CS0618 // Type or member is obsolete
            Recreate();
            System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("ar");
            Intent nextActivity = new Intent(this, typeof(MainActivity));
            StartActivity(nextActivity);
        }

        private void Onclick3(object sender, EventArgs e)
        {
            var locale = new Java.Util.Locale("tr");
            Android.Content.Res.Configuration conf = Resources.Configuration;
            conf.Locale = locale;
            DisplayMetrics dm = Resources.DisplayMetrics;
#pragma warning disable CS0618 // Type or member is obsolete
            Resources.UpdateConfiguration(conf, dm);
#pragma warning restore CS0618 // Type or member is obsolete
            Recreate();
            System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("tr");
            Intent nextActivity = new Intent(this, typeof(MainActivity));
            StartActivity(nextActivity);
        }
    }
}