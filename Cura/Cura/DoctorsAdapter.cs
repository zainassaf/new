﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Lang;
using static Android.Graphics.Bitmap;

namespace Cura
{
    class DoctorsAdapter: BaseAdapter
    {
       Context context;
        Doctor[] doc;

  public DoctorsAdapter(Context c, Doctor[] d)
        {
            context = c;
            doc = d;
        }

        public override int Count => doc.Length;
            

        public override Java.Lang.Object GetItem(int position)
        {
            return null;
        }

        public override long GetItemId(int position)
        {
            return 0;
        }

        // create a new ImageView for each item referenced by the Adapter
        public override View GetView(int position, View convertView, ViewGroup parent)
        {

            LayoutInflater inflater = (LayoutInflater)context.GetSystemService(Context.LayoutInflaterService);

            View grid;

            if (convertView == null)
            {
                grid = inflater.Inflate(Resource.Layout.DoctorLayout, null);

                ImageView imageView = grid.FindViewById<ImageView>(Resource.Id.image1);
                TextView nTextView = grid.FindViewById<TextView>(Resource.Id.textview1);
                TextView sTextView = grid.FindViewById<TextView>(Resource.Id.textview2);
                TextView cTextView = grid.FindViewById<TextView>(Resource.Id.textview3);
                
                
                var imageBitmap = GetImageBitmapFromUrl("https://www.rd.com/wp-content/uploads/2017/09/02_doctor_Insider-Tips-to-Choosing-the-Best-Primary-Care-Doctor_519507367_Stokkete-1200x1200.jpg");
                Bitmap bitmap_round = GetRoundedShape(imageBitmap);
                imageView.SetImageBitmap(bitmap_round);
               

                nTextView.Text = doc[position].FirstName +" "+ doc[position].LastName;
                sTextView.Text = doc[position].SpecialtyTitle_en;
                cTextView.Text = doc[position].ClassificationTitle_en;
                if (doc[position].AvailabilityStatusColor.Contains("#99CB00"))
                {
                    
                }
                else if (doc[position].AvailabilityStatusColor.Contains("#FACF00"))
                {

                }
                else if (doc[position].AvailabilityStatusColor.Contains("#F12828"))
                {

                }
            }
            else
            {
                grid = (View)convertView;
            }
            return grid;
        }
        private Bitmap GetImageBitmapFromUrl(string url)
        {
            Bitmap imageBitmap = null;

            using (var webClient = new WebClient())
            {
                var imageBytes = webClient.DownloadData(url);
                if (imageBytes != null && imageBytes.Length > 0)
                {
                    imageBitmap = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
                }
            }

            return imageBitmap;
        }
        public Bitmap GetRoundedShape(Bitmap scaleBitmapImage)
        {
            int targetWidth = 100;
            int targetHeight = 100;
            Bitmap targetBitmap = Bitmap.CreateBitmap(targetWidth,
                    targetHeight, Bitmap.Config.Argb8888);

            Canvas canvas = new Canvas(targetBitmap);
            Path path = new Path();
            path.AddCircle(((float)targetWidth - 1) / 2,
                    ((float)targetHeight - 1) / 2,
                    (System.Math.Min(((float)targetWidth),
                            ((float)targetHeight)) / 2),
                            Path.Direction.Ccw);

            canvas.ClipPath(path);
            Bitmap sourceBitmap = scaleBitmapImage;
            canvas.DrawBitmap(sourceBitmap,
                    new Rect(0, 0, sourceBitmap.Width,
                            sourceBitmap.Height),
                            new Rect(0, 0, targetWidth, targetHeight), null);
            return targetBitmap;
        }


    }

}
