﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Support.V7.App;
using RestSharp;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Cura
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        List<Doctor> doctor = new List<Doctor>();
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);
            RestClient client = new RestClient("http://api-dev.cura.healthcare/");
            RestRequest request = new RestRequest("MedicalCategoriesListing/193", Method.GET);
            request.RequestFormat = DataFormat.Json;
            var response = client.Execute<List<MedicalCategoryListingResponse>>(request);
            var content = response.Content;
            var eventResponse = JsonConvert.DeserializeObject<MedicalCategoryListingResponse>(content);
            
            foreach(var i in eventResponse.Result)
            {
               foreach(var j in i.Doctors)
                {
                    doctor.Add(j);
                }
                    
            }
            var gridview = FindViewById<GridView>(Resource.Id.gridView1);
            DoctorsAdapter d = new DoctorsAdapter(this, doctor.ToArray());
            gridview.Adapter = d;
            
        }
    }
}